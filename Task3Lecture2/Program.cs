﻿using System;
using System.Collections.Generic;

namespace Task3Lecture2
{
    class Program
    {
        static void Main(string[] args)
        {
            List<Person> people = new List<Person>()
            {
                new Person("Saint", "Peter"),
                new Person("Henrik", "Nordtug"),
                new Person("Petter", "Nordthug"),
                new Person("Petter", "Solberg"),
                new Person("Erna", "Solberg")
            };

            Console.Write("Search for person: ");
            string searchString = Console.ReadLine();

            //Adds partial matches
            List<Person> matches = people.FindAll(p => p.FullName
                                                          .ToLower()
                                                          .Contains(searchString));

            //Adds full matches
            matches.AddRange(people.FindAll(p => p.FullName
                                                    .ToLower()
                                                    .Equals(searchString)));

            Console.WriteLine("People found containing searchstring:");
            foreach (var person in matches)
            {
                Console.WriteLine(person.FullName);
            }
        }
    }

    struct Person
    {
        public string FirstName;
        public string LastName;
        public string FullName;

        public Person(string firstName, string lastName)
        {
            FirstName = firstName;
            LastName = lastName;
            FullName = $"{ firstName } { lastName }";
        }
    }
}
